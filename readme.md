#Pulling in a Twitter feed using JavaScript

###Topics explored
* JavaScript objects and object literals
* JSON data interchange format
	* Twitter API
* Consuming the Twitter API without script injection
	* noninject example
	* JSONP
* Consuming the Twitter API with script injection
	* Twitter username search example


### Example

We are going to build a page that pulls in the Twitter feed for @itpusc

* [http://www-scf.usc.edu/~dtang/ITP301/noninject-completed/simple.html](http://www-scf.usc.edu/~dtang/ITP301/noninject-completed/simple.html)
* [http://www-scf.usc.edu/~dtang/ITP301/search-completed/](http://www-scf.usc.edu/~dtang/ITP301/search-completed/)

### Object literals:


* Object literals are a way to create collections of key-value pairs. Typically these key-value pairs are related in some way. Think of it like an Array and how it has a collection of key-value pairs, where the key is the numeric index.
* You will see a lot of jQuery plugins using object literals as a way to pass in options to the plugin
* person object, tweet object (what would be some key value pairs?)
* how to access properties using dot syntax

```js
	var tweet = {
		text: 'I love JavaScript!',
		created: '2012-09-12'
	};


	tweet.text // 'I love JavaScript!'
	tweet.created // '2012-09-12'
```
### Twitter API

[http://api.twitter.com/1/statuses/user_timeline.json?screen_name=uscitp](http://api.twitter.com/1/statuses/user_timeline.json?screen_name=uscitp)

* query string variable: screen_name = uscitp
* data format = JSON

### What is JSON?

[JSON](http://www.json.org/) is a data interchange format that __IS__ JavaScript. Looking at the Twitter API endpoint, they want to share their data with other developers. In order to do that, they need to structure that data in a common format that is easy to work with. XML is one option, and JSON is another. If you are working with the data in JavaScript, then you typically want to use JSON.

JSON consists of arrays and object literals with a slightly stricter syntax. JSON is valid JavaScript.

Viewing JSON data in the browser can look kind of messy. Thankfully, there are a few tools to help us easily view and understand the structure of our JSON data.

* [http://www.jsoneditoronline.org/](http://www.jsoneditoronline.org/)
* Chrome extension JSONView 

###How do we bring this JSON data onto your page?

We need a way to make an HTTP request to this JSON page. From the browser, we can make HTTP requests using the following:

1. AJAX
2. script elements

__AJAX__ however is restricted by the Same Origin Policy, meaning that we can't make requests to pages outside of our domain. 

Now you might be wondering, why not just include the script reference into our page? We could certainly do that and it will work. Look at the __non-inject__ folder and you'll see an example of that. However, if you want to bring this data into our page at a later point in time (not on page load), then we will need a way to dynamically create a script element.

### JSONP

If you are working with JavaScript, you are probably aware that you can add new elements to the page. So let's dynamically create a script element with the Twitter API URL as its __src__ property and inject it into our page.

```js
	var script = document.createElement('script');
	script.src = "http://api.twitter.com/1/statuses/user_timeline.json?screen_name=uscitp&callback=createHTML";
	document.getElementsByTagName('head')[0].appendChild(script);
```

You'll notice that an extra query string variable __callback__ has been added to the Twitter URL. If we did not include this parameter, we would have injected this JSON data into our page, but there would have been no way of referencing it from within our page, thus making the data unaccessible and useless. Something like this:

```html
<script>
	[
		{"tweet": "some tweet"},
		{"tweet": "another tweet"},
		{"tweet": "yet another tweet"}	
	]
</script>
```

In order to reference this data, the Twitter API provides a way for us to specify a user defined function that will wrap around or __pad__ the JSON data. When this request finishes, this is what will be injected onto our page:

```js
	createHTML([{"tweet": "some tweet"},{"tweet": "another tweet"}]);
```

The createHTML function is called the __padding__, hence JSON with padding (JSONP). We can name this function anything we want as long as we define the function within our page. All of that JSON data then becomes an argument to the createHTML function. It may look kind of odd with all that data as an argument, but if you remember, JSON data is valid JavaScript so this will work as long as the JSON data is properly formatted (which it should be since it is coming from Twitter).

At this point, we can loop over the data from within our createHTML function, and create HTML elements from it to display on our page.
